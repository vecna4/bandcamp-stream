# bandcamp-stream

`bandcamp-stream` accepts a Bandcamp album URL and streams that album. This is useful if you want to preview an album without having to allow JavaScript on Bandcamp.

This script creates an M3U playlist file in `/tmp/bandcamp-stream/` and then seeks to open it with the default application.

## Dependencies

`wget`, a music player that supports M3U playlists and is set as the preferred application for that filetype

## Installing

This script can be installed by running the `install` script as root, or by copying `bandcamp-stream` to `/usr/bin/`

To add the manual for `bandcamp-stream` (if manually installing), also copy `bandcamp-stream.1` to `/usr/local/share/man/man1/`, then run `mandb` as root.

The `install` script does not remove these files after copying them, but it is safe to do so if you wish.

## Usage

`bandcamp-stream <URL>`

## Options

Currently, `bandcamp-stream` does not accept any options.

## Known Bugs

Right now, this program assumes that all tracks are streamable. In practice, this is often not true. When only some tracks are streamable, `bandcamp-stream` should still manage to grab the streams correctly; however, it may label them incorrectly.

## Special Thanks

Special thanks to Cabia Rangris, who wrote [`bandcamp-dl.sh`](https://gist.github.com/cab404/c47f43e7debe4b52502c), which I used as a reference.
